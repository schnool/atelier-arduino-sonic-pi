#include <ArduinoJson.h>

const int PIN_POT1 =  ;
const int PIN_POT2 = ;
int valPot1 = 0;
int valPot2 = ;

void setup()
{
  Serial.begin(9600);
  pinMode(PIN_POT1, );
  pinMode(PIN_POT2, );
}

void loop()
{
  //Creation d'un document Json
  const int capacity = JSON_OBJECT_SIZE(2);
  StaticJsonDocument<capacity> doc;

  //Lecture de la valeur des potentiomètres
  valPot1 = analogRead();
  valPot2 = analogRead();

  //Ajustement de la valeur aux notes dans Sonic-pi
  valPot1 = map(valPot1, 0, 1023, 40, 80);
  valPot2 = map(, 0, 1023, 40, 80);

  //Ajout de la note dans le document Json
  doc["note1"] = ;
  doc[""] = valPot2;

  //Serialization et envoi des données sur le port série
  serializeJson(doc, Serial);
  Serial.println();

  delay(1000);
}
