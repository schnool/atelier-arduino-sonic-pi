import serial
import json
from pythonosc import osc_message_builder
from pythonosc import udp_client

# Début de la communication série
ser = serial.Serial('/dev/ttyUSB0', '9600')

# Configuration de l'envoie de message osc
sender = udp_client.SimpleUDPClient('127.0.0.1', 4559)

# Lecture du port série
while True:
    if(ser.in_waiting > 0)
        data = ser.readline().decode('utf-8')
        print(data)
        jData = json.loads(data)
        note1 = jData['note1']
        note2 = jData['note2']
        sender.send_message('/note1', note1)
        sender.send_message('/note2', note2)
